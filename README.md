# Sprawozdanie z fizyki (ćwiczenie O1)

### Treść ćwiczenia
[Ćwiczenie O1](https://ftims.pg.edu.pl/documents/10673/46008532/cwiczenieO1.pdf)

### Kompilacja
`pdflatex fizyka.tex`

### Rezultat
[Plik fizyka.pdf](https://gitlab.com/marcel2012/fizyka-sprawozdanie-nr-3/-/jobs/artifacts/master/raw/fizyka.pdf?job=build)
